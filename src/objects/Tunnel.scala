package objects

import java.awt.Image

import utils.Res
import utils.Utils

class Tunnel (
    override var x: Int,
    override val y: Int
) extends GameObjectImpl() {

  override val width: Int = 43
  override val height: Int = 65

  override val imgObj: Image = Utils.getImage(Res.IMG_TUNNEL)
}