package objects

import java.awt.Image

import utils.{Res, Utils}

class Piece (
    override var x: Int,
    override val y: Int
) extends GameObjectImpl() with Runnable {

    override val width: Int = 30
    override val height: Int = 30

  override val imgObj: Image = Utils.getImage(Res.IMG_PIECE1)

  val PAUSE: Int = 10
  val FLIP_FREQUENCY: Int = 100
  var counter: Int = 0




  def imageOnMovement(): Image = {
    this.counter = this.counter + 1
  Utils.getImage(if(this.counter % FLIP_FREQUENCY == 0) Res.IMG_PIECE1 else Res.IMG_PIECE2)
}

  @Override
  def run(): Unit = {
      while (true) {
        this.imageOnMovement()
        try {
        Thread.sleep(PAUSE)
        } catch {
          case e: InterruptedException => e.printStackTrace()
        }
      }
    }
}
