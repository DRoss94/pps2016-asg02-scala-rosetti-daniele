package objects
import java.awt.Image

import utils.{Res, Utils}

class Castle (
              override var x: Int,
              override val y: Int
) extends GameObjectImpl{

  override val width: Int = 124
  override val height: Int = 150

  override val imgObj: Image = Utils.getImage(Res.IMG_CASTLE_FINAL)
}
