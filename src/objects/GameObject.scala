package objects


import game.Window
import java.awt.Image

trait GameObject {
    def x: Int
    def y: Int
    def width: Int
    def height: Int
    def imgObj: Image

    def getX(): Int
    def getY(): Int
    def getWidth(): Int
    def getHeight(): Int
    def getImgObj(): Image
    def move(): Unit
}

abstract class GameObjectImpl() extends GameObject {

    var x: Int
    val y: Int
    val width: Int
    val height: Int
    val imgObj: Image

    def getX(): Int = x


    def getY(): Int = y


    def getWidth(): Int = width


    def getHeight(): Int = height


    def getImgObj(): Image = imgObj


    def move(): Unit = this match {
        case _ if (Window.getInstance().getScene().getxPos() >= 0) => this.x = this.x - Window.getInstance().getScene().getMov()
    }
}