package point

import javax.swing.JLabel


class PointManager extends JLabel {

  val CONSTANT_PART = "Score: "
  val START_VALUE = 0
  val CENTER_X = 318
  val CENTER_Y = 5
  val DEFAULT_WIDTH = 100
  val DEFAULT_HEIGHT = 20

  var value = START_VALUE

  this.setText(CONSTANT_PART + value)
  this.setBounds(CENTER_X, CENTER_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT)

  def scoredPoint(): Unit = {
    value=value+1
    this.setText(CONSTANT_PART + value)
  }
}
