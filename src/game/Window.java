package game;

import javax.swing.*;

import java.awt.*;

import static game.Main.*;
import static java.lang.System.exit;


public class Window {

    private static Window ourInstance = new Window();

    public static Window getInstance() {
        return ourInstance;
    }

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";
    public static Platform scene;
    private static JFrame finestra;

    private Window() {
    }

    public void start() {
        finestra = new JFrame(WINDOW_TITLE);
        finestra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        finestra.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        finestra.setLocationRelativeTo(null);
        finestra.setResizable(true);
        finestra.setAlwaysOnTop(true);

        scene = new Platform();
        finestra.setContentPane(scene);
        finestra.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();

    }

    public void close() {
        Runtime.getRuntime().exit(0);
    }

    public Platform getScene() {
        return scene;
    }
}
