package game;

public class Refresh implements Runnable {

    private final int PAUSE = 3;

    public void run() {
        while (true) {
            Window.getInstance().getScene().repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

} 
