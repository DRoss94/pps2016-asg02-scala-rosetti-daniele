package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    private static final int BACKGROUND_POS_X1 = -50;
    private static final int BACKGROUND_POS_X2 = 750;
    @Override
    public void keyPressed(KeyEvent e) {
        if (Window.getInstance().getScene().getMario().isAlive()) {

            switch (e.getKeyCode()) {
                case KeyEvent.VK_RIGHT:
                    this.sceneMovement(-1,0,true);
                    break;
                case KeyEvent.VK_LEFT:
                    this.sceneMovement(4601, 4600, false);
                    break;
                case KeyEvent.VK_UP:
                    Window.getInstance().getScene().getMario().setJumping(true);
                    Audio.playSound("/resources/audio/jump.wav");
                    break;
            }

        }
    }

    private void sceneMovement(int initialXPos, int terminalXPos, boolean rightDirection) {
        if (Window.getInstance().getScene().getxPos() == initialXPos) {
            Window.getInstance().getScene().setxPos(terminalXPos);
            Window.getInstance().getScene().setBackground1PosX(BACKGROUND_POS_X1);
            Window.getInstance().getScene().setBackground2PosX(BACKGROUND_POS_X2);
        }
        Window.getInstance().getScene().getMario().setMoving(true);
        Window.getInstance().getScene().getMario().setToRight(rightDirection);
        if(rightDirection) {
            Window.getInstance().getScene().setMov(1); // si muove verso destra
        } else {
            Window.getInstance().getScene().setMov(-1); // si muove verso sinistra
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        Window.getInstance().getScene().getMario().setMoving(false);
        Window.getInstance().getScene().setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}