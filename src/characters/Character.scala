package characters

import java.awt.Image

trait Character {
  def walk(name:String, frequency: Int): Image
}
