package characters

import java.awt.Image

import game.Window
import objects.GameObject
import utils.{Res, Utils}

abstract class BasicCharacter (
  var x: Int,
  var y: Int,
  var width: Int,
  var height: Int
) extends Character {

  val PROXIMITY_MARGIN: Int = 10
  var moving: Boolean = false
  var toRight: Boolean = true
  var counter: Int = 0
  var alive: Boolean = true

  def getX(): Int = {
    x
  }

  def getY(): Int = {
    y
  }

  def getWidth(): Int = {
     width
  }

  def getHeight(): Int = {
     height
  }

  def isAlive(): Boolean = {
     alive
  }

  def isToRight(): Boolean = {
    toRight
  }

  def setAlive(alive: Boolean): Unit = {
    this.alive = alive
  }

  def setX(x: Int): Unit = {
    this.x = x
  }

  def setY(y: Int): Unit = {
    this.y = y
  }

  def setMoving(moving: Boolean): Unit = {
    this.moving = moving
  }

  def setToRight(toRight: Boolean): Unit = {
    this.toRight = toRight
  }

  override def walk(name: String, frequency: Int): Image = {
    val str: String = Res.IMG_BASE + name +
      (if (!this.moving || (this.counter + 1) % frequency == 0) Res.IMGP_STATUS_ACTIVE else Res.IMGP_STATUS_NORMAL) +
      (if (this.toRight) Res.IMGP_DIRECTION_DX else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT
    Utils.getImage(str)
  }

   def move(): Unit = {
     if (Window.getInstance().getScene().getxPos() >= 0) this.x = this.x - Window.getInstance().getScene().getMov()
  }

   def hitAhead(og: GameObject): Boolean = og match {
     case _ if this.x + this.width < og.getX() || this.x + this.width > og.getX() + 5 ||
       this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight() => false
     case _ => true
  }

  def hitBack(og: GameObject): Boolean = og match {
    case _ if this.x > og.getX() + og.getWidth() || this.x + this.width < og.getX() + og.getWidth() - 5 ||
      this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight() => false
    case _ => true
  }

  def hitBelow(og: GameObject): Boolean = og match {
    case _ if this.x + this.width < og.getX() + 5 || this.x > og.getX() + og.getWidth() - 5 ||
      this.y + this.height < og.getY() || this.y + this.height > og.getY() + 5 => false
    case _ => true
  }

  def hitAbove(og: GameObject): Boolean = og match {
    case _ if this.x + this.width < og.getX() + 5 || this.x > og.getX() + og.getWidth() - 5 ||
      this.y < og.getY() + og.getHeight() || this.y > og.getY() + og.getHeight() + 5 => false
    case _  => true
  }

  def  hitAhead(pers: BasicCharacter): Boolean = {
    def innerFun(): Boolean = pers match {
      case _ if this.x + this.width < pers.getX() || this.x + this.width > pers.getX() + 5 ||
        this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight() => false
      case _ => true
    }
    pers match {
      case _ if this.isToRight() => innerFun()
      case _ => false
    }
  }

  def hitBack(pers: BasicCharacter): Boolean = pers match {
    case _ if this.x > pers.getX() + pers.getWidth() || this.x + this.width < pers.getX() + pers.getWidth() - 5 ||
      this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight() => false
    case _ => true
  }

  def hitBelow(pers: BasicCharacter): Boolean = pers match {
    case _ if this.x + this.width < pers.getX() || this.x > pers.getX() + pers.getWidth() ||
      this.y + this.height < pers.getY() || this.y + this.height > pers.getY() => false
    case _ => true
  }

  def isNearby(pers: BasicCharacter): Boolean = pers match {
    case _ if (this.x > pers.getX() - PROXIMITY_MARGIN && this.x < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN)
      || (this.x + this.width > pers.getX() - PROXIMITY_MARGIN && this.x + this.width < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN) => true
    case _ => false
  }

  def isNearby(obj: GameObject): Boolean = obj match {
    case _ if (this.x > obj.getX() - PROXIMITY_MARGIN && this.x < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
      (this.x + this.width > obj.getX() - PROXIMITY_MARGIN && this.x + this.width < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) =>  true
    case _ => false
  }

  //template method
  def contact(person: BasicCharacter): Unit

  def contact(gameObject: GameObject): Unit
}